<?php

class TycoonAuthorizeNetCIMPayMethod extends TycoonCreditCardPayMethod {

  /**
   * Information about options for all kinds of purposes will be held here.
   */
  protected function option_definition() {
    $options = parent::option_definition();

    $options['login_id'] = array('default' => '');
    $options['transaction_key'] = array('default' => '');
    $options['test'] = array('default' => TRUE);
    $options['sandbox'] = array('default' => FALSE);
    $options['email_merchant'] = array('default' => TRUE);
    $options['email_customer'] = array('default' => FALSE);

    return $options;
  }

  /**
   * Provide a form to edit options for this plugin.
   */
  function options_form(&$form, &$form_state) {
    $form['login_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Login ID'),
      '#default_value' => $this->options['login_id'],
    );
    $form['transaction_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Transaction key'),
      '#default_value' => $this->options['transaction_key'],
    );
    $form['test'] = array(
      '#type' => 'checkbox',
      '#title' => t('Test mode'),
      '#default_value' => !empty($this->options['test']),
    );
    $form['sandbox'] = array(
      '#type' => 'checkbox',
      '#title' => t('Sandbox mode'),
      '#default_value' => !empty($this->options['sandbox']),
    );
    $form['email_merchant'] = array(
      '#type' => 'checkbox',
      '#title' => t('E-mail receipt to merchant'),
      '#default_value' => !empty($this->options['email_merchant']),
    );
    $form['email_customer'] = array(
      '#type' => 'checkbox',
      '#title' => t('E-mail receipt to customer'),
      '#default_value' => !empty($this->options['email_customer']),
    );
  }

  /**
   * Validate the options form.
   */
  function options_validate(&$form, &$form_state) {
    $values = $form_state['values'];

    if (!empty($values['login_id']) && strlen($values['login_id']) < 8) {
      form_set_error('login_id', t('An Authorize.Net login must be at least 8 characters.'));
    }
    if (!empty($values['transaction_key']) && strlen($values['transaction_key']) != 16) {
      form_set_error('transaction_key', t('An Authorize.Net transaction key must be 16 characters.'));
    }
  }

  /**
   * Checks data types and required.
   * TODO: line items!!!
   */
  function validate(&$transaction, &$data) {
    module_load_include('inc', 'tycoon_authorizenet', 'tycoon_authorizenet');
    if (!_tycoon_authorizenet_sdk(TRUE)) {
      watchdog('tycoon', 'Authorize.net SDK not found.');
      $errors = array(array(
        'message' => 'Something went wrong.',
        'data' => array(
          'key' => 'cc_number',
        ),
      ));
      throw new TycoonActionException('Credit card transaction failed.', array(), $errors);
    }

    parent::validate($transaction, $data);

    $request = $this->authorizenet_aim_request();
    TycoonAuthorizeNetAIMPayMethod::authorizenet_aim_customer_data($request, $transaction);

//    $request->tax                = $tax = "0.00"; ???
    $trans = array(
      //'email_merchant' => !empty($this->options['email_merchant']) ? 'true' : 'false',
      'email_customer' => !empty($this->options['email_customer']) ? 'true' : 'false',
    );
    $request->setFields($trans);
    $request->setCustomField('x_email_merchant', !empty($this->options['email_merchant']) ? 'true' : 'false');

    $order = array(
      'description' => $data['description'],
      'invoice_num' => $transaction->txid,
    );
    $request->setFields($order);

    // AUTH_ONLY
    $response = $request->authorizeOnly($transaction->total, $data['cc_number'], $data['cc_date']);
    $data['transaction_id'] = $response->transaction_id;

    return TycoonAuthorizeNetAIMPayMethod::authorizenet_aim_response_status($response);
  }

  function process(&$transaction, &$data) {
    parent::process($transaction, $data);

    $request = $this->authorizenet_aim_request();
    TycoonAuthorizeNetAIMPayMethod::authorizenet_aim_customer_data($request, $transaction);

//    $request->tax                = $tax = "0.00"; ???
    $trans = array(
      //'email_merchant' => !empty($this->options['email_merchant']) ? 'true' : 'false',
      'email_customer' => !empty($this->options['email_customer']) ? 'true' : 'false',
    );
    $request->setFields($trans);
    $request->setCustomField('x_email_merchant', !empty($this->options['email_merchant']) ? 'true' : 'false');

    // TODO: is invoice num missing for some reason?
    $order = array(
      'description' => $data['description'],
      'invoice_num' => $transaction->txid,
    );
    $request->setFields($order);

    if (!empty($data['transaction_id'])) {
      $request->trans_id = $data['transaction_id'];
    }

    if ($transaction->state == 'validated' && $data['transaction_id']) {
      // PRIOR_AUTH_CAPTURE
      $response = $request->priorAuthCapture($data['transaction_id'], $transaction->total);
    }
    else {
      // AUTH_CAPTURE
      $response = $request->authorizeAndCapture($transaction->total, $data['cc_number'], $data['cc_date']);
      $data['transaction_id'] = $response->transaction_id;
    }

    TycoonAuthorizeNetAIMPayMethod::authorizenet_aim_response_status($response);

    // Success!!!
    $transaction->total_paid = $response->amount;

    return TRUE;
  }

  function save($aid, $data) {
    // TODO: maybe format data?
    parent::save($aid, $data);
  }

  function authorizenet_aim_request() {
    $request = new AuthorizeNetAIM($this->options['login_id'], $this->options['transaction_key']);
    $request->setSandbox($this->options['sandbox']);
    return $request;
  }

  protected static function authorizenet_aim_customer_data(&$request, $transaction) {
    $customer = (object)array();
    $customer->first_name = $transaction->first_name;
    $customer->last_name = $transaction->last_name;
    $customer->company = $transaction->company;
    $customer->address = $transaction->street1;
    $customer->city = $transaction->city;
    $customer->state = $transaction->province;
    $customer->zip = $transaction->postal_code;
    $customer->country = $transaction->country;
    $customer->phone = $transaction->phone;
    //$customer->fax = "415-555-5556";
    $customer->email = $transaction->mail;
    $customer->cust_id = $transaction->uid;
    $customer->customer_ip = ip_address();

    $request->setFields($customer);
  }

  protected static function authorizenet_aim_response_status($response) {
    if ($response->error) {
      $errors = array(array(
        'message' => 'Credit card error: @message <em>(Code: @code, Subcode: @subcode)</em>!avs!cvv',
        'args' => array(
          '@message' => $response->response_reason_text,
          '@code' => $response->response_code,
          '@subcode' => $response->response_subcode,
          '!avs' => !empty($response->avs_response) && ($avs = tycoon_authorizenet_aim_avs_response($response->avs_response)) ? t('<br />AVS match: @avs', array('@avs' => $avs)) : '',
          '!cvv' => !empty($response->card_code_response) && ($cvv = tycoon_authorizenet_aim_cvv_response($response->card_code_response)) ? t('<br />CVV match: @cvv', array('@cvv' => $cvv)) : '',
        ),
        'data' => array(
          'key' => 'cc_number',
        ),
      ));

      throw new TycoonActionException('Credit card transaction failed.', array(), $errors);
    }
    return TRUE;
  }
}
