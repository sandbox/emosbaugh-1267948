<?php

function _tycoon_authorizenet_sdk($include = FALSE, $file = 'anet_php_sdk/AuthorizeNet.php') {
  $directory = 'libraries';
  $searchdir = array();
  $config = conf_path();

  // Always search sites/all/libraries.
  $searchdir[] = 'sites/all/' . $directory;

  // Also search sites/<domain>/libraries.
  if (file_exists("$config/$directory")) {
    $searchdir[] = "$config/$directory";
  }

  // Retrieve list of directories.
  // @todo Core: Allow to scan for directories.
  $directories = array();
  foreach ($searchdir as $dir) {
    if (is_dir($dir) && file_exists("$dir/$file")) {
      if ($include) {
        include_once("$dir/$file");
      }
      return TRUE;
    }
  }

  return FALSE;
}
